import { Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { UploadService } from './service/upload.service';
import { HomeComponent } from './home/home.component';

@Component({
  providers: [HomeComponent],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  title = 'ng5';
  file;
  @ViewChild('fileinput')
  fileinput: ElementRef;
  @ViewChild('fileinputdummy')
  fileinputdummy: ElementRef;

  constructor(private uploadService: UploadService, private home: HomeComponent){}

  incomingfile(event) {
    this.file = event.target.files[0];
    this.uploadService.setFile(this.file);
    this.fileinputdummy.nativeElement.value = this.file.name;
    console.log('file selected')
    console.log(this.file);
  }

  clear() {
    this.home.Upload()
    this.fileinput.nativeElement.value = "";
    this.fileinputdummy.nativeElement.value = "";
    //console.log(this.file_element.nativeElement);
  }

}
