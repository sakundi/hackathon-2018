import os
import pickle
import json

model = pickle.load(open(os.path.join(os.getcwd(), 'model', 'emp_attr_pred.pkl'), 'rb'))

#inputJson = '{"Age":26, "DistanceFromHome":25, "EnvironmentSatisfaction":1, "Gender":0, "JobSatisfaction":3, "MaritalStatus":0, "MonthlyIncome":2293, "PercentSalaryHike":12, "PerformanceRating":3, "TotalWorkingYears":1, "WorkLifeBalance":2, "YearsAtCompany":1, "YearsSinceLastPromotion":0}'
#inputJson = json.dumps(a)
#multiJson = '{"1" : {"Age":26, "DistanceFromHome":25, "EnvironmentSatisfaction":1, "Gender":0, "JobSatisfaction":3, "MaritalStatus":0, "MonthlyIncome":2293, "PercentSalaryHike":12, "PerformanceRating":3, "TotalWorkingYears":1, "WorkLifeBalance":2, "YearsAtCompany":1, "YearsSinceLastPromotion":0},"2" : {"Age":26, "DistanceFromHome":25, "EnvironmentSatisfaction":1, "Gender":0, "JobSatisfaction":3, "MaritalStatus":0, "MonthlyIncome":2293, "PercentSalaryHike":12, "PerformanceRating":3, "TotalWorkingYears":1, "WorkLifeBalance":2, "YearsAtCompany":1, "YearsSinceLastPromotion":0}}'
#newInput = '{"1" : {"Age":50, "BusinessTravel":0, "Department":0, "DistanceFromHome":20, "EnvironmentSatisfaction":4, "Gender":1, "JobInvolvement":2, "JobSatisfaction":1, "MaritalStatus":2, "MonthlyIncome":10854, "NumCompaniesWorked":4, "OverTime":1, "PercentSalaryHike":13, "TrainingTimesLastYear":3, "WorkLifeBalance":3, "YearsInCurrentRole":2}, "2" : {"Age":50, "BusinessTravel":0, "Department":0, "DistanceFromHome":20, "EnvironmentSatisfaction":4, "Gender":1, "JobInvolvement":2, "JobSatisfaction":1, "MaritalStatus":2, "MonthlyIncome":10854, "NumCompaniesWorked":4, "OverTime":1, "PercentSalaryHike":13, "TrainingTimesLastYear":3, "WorkLifeBalance":3, "YearsInCurrentRole":2}}'
#inputJson = ['{"Age":50, "BusinessTravel":0, "Department":0, "DistanceFromHome":20, "EnvironmentSatisfaction":4, "Gender":1, "JobInvolvement":2, "JobSatisfaction":1, "MaritalStatus":2, "MonthlyIncome":10854, "NumCompaniesWorked":4, "OverTime":1, "PercentSalaryHike":13, "TrainingTimesLastYear":3, "WorkLifeBalance":3, "YearsInCurrentRole":2}', '{"Age":50, "BusinessTravel":0, "Department":0, "DistanceFromHome":20, "EnvironmentSatisfaction":4, "Gender":1, "JobInvolvement":2, "JobSatisfaction":1, "MaritalStatus":2, "MonthlyIncome":10854, "NumCompaniesWorked":4, "OverTime":1, "PercentSalaryHike":13, "TrainingTimesLastYear":3, "WorkLifeBalance":3, "YearsInCurrentRole":2}']


def getAttrition(inputJson):
#    inputJson = json.loads(inputJson)
#    print(inputJson)
    modelInput = []
    for v in inputJson:
        modelInput.append([v['Age'], v['BusinessTravel'], v['Department'], v['DistanceFromHome'], v['EnvironmentSatisfaction'], v['Gender'], v['JobInvolvement'], v['JobSatisfaction'], v['MaritalStatus'], v['MonthlyIncome'], v['NumCompaniesWorked'], v['OverTime'], v['PercentSalaryHike'],v['TrainingTimesLastYear'],v['WorkLifeBalance'],v['YearsInCurrentRole']])
        
    val = model.predict_proba(modelInput)
    counter = 0
    for v in inputJson:
        v['AttritionValue'] = val[counter][1]
        
        str = ''
        if val[counter][1]>=0.6:
            #Age group less than 31
            if v['Age'] <= 30:
                if v['DistanceFromHome'] > 9:
                    str = str + "DistanceFromHome,"
                if v['EnvironmentSatisfaction'] < 3:
                    str = str + "EnvironmentSatisfaction,"
                if v['JobInvolvement'] < 3:
                    str = str + "JobInvolvement," 
                if v['JobSatisfaction'] < 3:
                    str = str + "JobSatisfaction,"                    
                if v['MonthlyIncome'] < 4000:
                    str = str + "MonthlyIncome,"                   
                if v['PercentSalaryHike'] < 15:
                    str = str + "PercentSalaryHike,"
                if v['WorkLifeBalance'] < 3:
                    str = str + "WorkLifeBalance,"
                if v['YearsInCurrentRole'] > 3:
                    str = str + "YearsInCurrentRole,"
            #Age group 31 to 40
            if v['Age'] > 30 and v['Age'] <= 40:
                if v['DistanceFromHome'] > 9:
                    str = str + "DistanceFromHome,"
                if v['EnvironmentSatisfaction'] < 2:
                    str = str + "EnvironmentSatisfaction,"
                if v['JobInvolvement'] < 2:
                    str = str + "JobInvolvement," 
                if v['JobSatisfaction'] < 2:
                    str = str + "JobSatisfaction,"                    
                if v['MonthlyIncome'] < 6000:
                    str = str + "MonthlyIncome,"                   
                if v['PercentSalaryHike'] < 15:
                    str = str + "PercentSalaryHike,"
                if v['WorkLifeBalance'] < 3:
                    str = str + "WorkLifeBalance,"
                if v['YearsInCurrentRole'] > 5:
                    str = str + "YearsInCurrentRole,"
            #Age group 41 to 50
            if v['Age'] > 40 and v['Age'] <= 50:
                if v['DistanceFromHome'] > 9:
                    str = str + "DistanceFromHome,"
                if v['EnvironmentSatisfaction'] < 3:
                    str = str + "EnvironmentSatisfaction,"
                if v['JobInvolvement'] < 3:
                    str = str + "JobInvolvement," 
                if v['JobSatisfaction'] < 3:
                    str = str + "JobSatisfaction,"                    
                if v['MonthlyIncome'] < 9000:
                    str = str + "MonthlyIncome,"                   
                if v['PercentSalaryHike'] < 15:
                    str = str + "PercentSalaryHike,"
                if v['WorkLifeBalance'] < 3:
                    str = str + "WorkLifeBalance,"
                if v['YearsInCurrentRole'] > 5:
                    str = str + "YearsInCurrentRole,"
            #Age group greater than 50
            if v['Age'] > 50:
                if v['DistanceFromHome'] > 9:
                    str = str + "DistanceFromHome,"
                if v['EnvironmentSatisfaction'] < 2:
                    str = str + "EnvironmentSatisfaction,"
                if v['JobInvolvement'] < 3:
                    str = str + "JobInvolvement," 
                if v['JobSatisfaction'] < 2:
                    str = str + "JobSatisfaction,"                    
                if v['MonthlyIncome'] < 10500:
                    str = str + "MonthlyIncome,"                   
                if v['PercentSalaryHike'] < 15:
                    str = str + "PercentSalaryHike,"
                if v['WorkLifeBalance'] < 3:
                    str = str + "WorkLifeBalance,"
                if v['YearsInCurrentRole'] > 5:
                    str = str + "YearsInCurrentRole,"                
        
        v['Reason'] = str[:-1]
        
        counter = counter + 1
    
#    print(inputJson1)
    return inputJson
        

    #input_data = [[inputData['Age'], inputData['DistanceFromHome'], inputData['EnvironmentSatisfaction'], inputData['Gender'], inputData['JobSatisfaction'], inputData['MaritalStatus'], inputData['MonthlyIncome'], inputData['PercentSalaryHike'], inputData['PerformanceRating'], inputData['TotalWorkingYears'], inputData['WorkLifeBalance'], inputData['YearsAtCompany'], inputData['YearsSinceLastPromotion']]]
    #val = model.predict_proba(input_data)[0][1]
    #return '{AttritionValue:',val, '}'

#print(getAttrition(json.dumps(inputJson)))

#def test(multiJson):
#    multiJson = json.loads(multiJson)
#    for (k, v) in multiJson.items():
#        print("Key: " + k)
#        print("Value: " + str(v))
#
#test(multiJson)

    
#print(model.predict(input_data))
    #print(model.predict_proba(input_data))

##input_data = [[28,15,1,0,3,2,2207,16,3,4,2,4,2]]
#input_data = [[26,1,4,0,3,0,2293,12,3,1,2,1,0]]
##input_data = [[48,2,2,0,2,1,8120,12,3,12,3,2,2]]
##output_data = model.predict(input_data)
#print(model.predict(input_data))
#print(model.predict_proba(input_data))
#print(model.predict_proba(input_data)[0][1])