import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import {Observable} from 'rxjs/Observable';
 
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable()
export class PredictionService {
 
    constructor(private http:HttpClient) {}
     data
    // Uses http.get() to load data from a single API endpoint
    getPrediction(body) {
        console.log('in service class')
        this.data = this.http.post('http://localhost:9080/predict', body);
        console.log('data==' + this.data)
        return this.data
    }
}