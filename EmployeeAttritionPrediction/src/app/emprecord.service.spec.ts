import { TestBed } from '@angular/core/testing';

import { EmprecordService } from './emprecord.service';

describe('EmprecordService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmprecordService = TestBed.get(EmprecordService);
    expect(service).toBeTruthy();
  });
});
