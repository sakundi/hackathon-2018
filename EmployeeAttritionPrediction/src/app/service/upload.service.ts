import {Injectable} from '@angular/core';
import { EmprecordService } from '../emprecord.service';
import * as XLSX from 'xlsx';

@Injectable()
export class UploadService {
    predicitons: any=[];
    predictionJson: any=[];
    predicitonarray;
    arrayBuffer:any;
    file;
    constructor(private emprecordService:EmprecordService){}

    setFile(file) {
        this.file = file;
    }
    Upload() {
        var worksheet;
        let fileReader = new FileReader();
          fileReader.onload = (e) => {
              this.arrayBuffer = fileReader.result;
              var data = new Uint8Array(this.arrayBuffer);
              var arr = new Array();
              for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
              var bstr = arr.join("");
              var workbook = XLSX.read(bstr, {type:"binary"});
              var first_sheet_name = workbook.SheetNames[0];
              worksheet = workbook.Sheets[first_sheet_name];
              // console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
              this.predicitons = XLSX.utils.sheet_to_json(worksheet,{raw:true});
              var count=0;
              this.predictionJson = this.predicitons;
              for(var predict in this.predictionJson){
                //console.log(this.predicitons[predict]);
                //this.predictionJson[count] = {};
                this.predictionJson[count]["Age"] = this.predicitons[predict].Age;
                // **************************BusinessTravel*************************
                if(this.predicitons[predict].BusinessTravel=="Travel_Rarely"){
                  this.predictionJson[count]["BusinessTravel"] = 0;
                }
                else if(this.predicitons[predict].BusinessTravel=="Travel_Frequently"){
                  this.predictionJson[count]["BusinessTravel"] = 1;
                }
                else if(this.predicitons[predict].BusinessTravel=="Non-Travel"){
                  this.predictionJson[count]["BusinessTravel"] = 2;
                }
                // *************************Department**********************
                if(this.predicitons[predict].Department=="Sales"){
                  this.predictionJson[count]["Department"] = 0;
                }
                else if(this.predicitons[predict].Department=="Research & Development"){
                  this.predictionJson[count]["Department"] = 1;
                }
                else if(this.predicitons[predict].Department=="Human Resources"){
                  this.predictionJson[count]["Department"] = 2;
                }
                // ************************EducationField********************
                if(this.predicitons[predict].EducationField=="Life Sciences"){
                  this.predictionJson[count]["EducationField"] = 0;
                }
                else if(this.predicitons[predict].EducationField=="Other"){
                  this.predictionJson[count]["EducationField"] = 1;
                }
                else if(this.predicitons[predict].EducationField=="Medical"){
                  this.predictionJson[count]["EducationField"] = 2;
                }
                else if(this.predicitons[predict].EducationField=="Marketing"){
                  this.predictionJson[count]["EducationField"] = 3;
                }
                else if(this.predicitons[predict].EducationField=="Technical Degree"){
                  this.predictionJson[count]["EducationField"] = 4;
                }
                else if(this.predicitons[predict].EducationField=="Human Resources"){
                  this.predictionJson[count]["EducationField"] = 5;
                }
                // *************************MaritalStatus**********************
              if(this.predicitons[predict].MaritalStatus=="Single"){
                this.predictionJson[count]["MaritalStatus"] = 0;
              }
              else if(this.predicitons[predict].MaritalStatus=="Married"){
                this.predictionJson[count]["MaritalStatus"] = 1;
              }
              else if(this.predicitons[predict].MaritalStatus=="Divorced"){
                this.predictionJson[count]["MaritalStatus"] = 2;
              }

              // **************************Gender**********************
              if(this.predicitons[predict].Gender=="Female"){
                this.predictionJson[count]["Gender"] = 0;
              }
              else if(this.predicitons[predict].Gender=="Male"){
                this.predictionJson[count]["Gender"] = 1;
              }
              // ************************JobRole********************
              if(this.predicitons[predict].JobRole=="Sales Executive"){
                this.predictionJson[count]["JobRole"] = 0;
              }
              else if(this.predicitons[predict].JobRole=="Research Scientist"){
                this.predictionJson[count]["JobRole"] = 1;
              }
              else if(this.predicitons[predict].JobRole=="Laboratory Technician"){
                this.predictionJson[count]["JobRole"] = 2;
              }
              else if(this.predicitons[predict].JobRole=="Manufacturing Director"){
                this.predictionJson[count]["JobRole"] = 3;
              }
              else if(this.predicitons[predict].JobRole=="Healthcare Representative"){
                this.predictionJson[count]["JobRole"] = 4;
              }
              else if(this.predicitons[predict].JobRole=="Manager"){
                this.predictionJson[count]["JobRole"] = 5;
              }
              else if(this.predicitons[predict].JobRole=="Sales Representative"){
                this.predictionJson[count]["JobRole"] = 6;
              }
              else if(this.predicitons[predict].JobRole=="Research Director"){
                this.predictionJson[count]["JobRole"] = 7;
              }
              else if(this.predicitons[predict].JobRole=="Human Resources"){
                this.predictionJson[count]["JobRole"] = 8;
              }
              // ************************OverTime********************
              if(this.predicitons[predict].OverTime=="Yes"){
                this.predictionJson[count]["OverTime"] = 1;
              }
              else if(this.predicitons[predict].OverTime=="No"){
                this.predictionJson[count]["OverTime"] = 0;
              }
              // ************************EnvironmentSatisfaction********************
              if(this.predicitons[predict].EnvironmentSatisfaction=="Low"){
                this.predictionJson[count]["EnvironmentSatisfaction"] = 1;
              }
              else if(this.predicitons[predict].EnvironmentSatisfaction=="Medium"){
                this.predictionJson[count]["EnvironmentSatisfaction"] = 2;
              }
              else if(this.predicitons[predict].EnvironmentSatisfaction=="High"){
                this.predictionJson[count]["EnvironmentSatisfaction"] = 3;
              }
              else if(this.predicitons[predict].EnvironmentSatisfaction=="Very High"){
                this.predictionJson[count]["EnvironmentSatisfaction"] = 4;
              }
              // ************************JobInvolvement********************
              if(this.predicitons[predict].JobInvolvement=="Low"){
                this.predictionJson[count]["JobInvolvement"] = 1;
              }
              else if(this.predicitons[predict].JobInvolvement=="Medium"){
                this.predictionJson[count]["JobInvolvement"] = 2;
              }
              else if(this.predicitons[predict].JobInvolvement=="High"){
                this.predictionJson[count]["JobInvolvement"] = 3;
              }
              else if(this.predicitons[predict].JobInvolvement=="Very High"){
                this.predictionJson[count]["JobInvolvement"] = 4;
              }
              // ************************JobSatisfaction********************
              if(this.predicitons[predict].JobSatisfaction=="Low"){
                this.predictionJson[count]["JobSatisfaction"] = 1;
              }
              else if(this.predicitons[predict].JobSatisfaction=="Medium"){
                this.predictionJson[count]["JobSatisfaction"] = 2;
              }
              else if(this.predicitons[predict].JobSatisfaction=="High"){
                this.predictionJson[count]["JobSatisfaction"] = 3;
              }
              else if(this.predicitons[predict].JobSatisfaction=="Very High"){
                this.predictionJson[count]["JobSatisfaction"] = 4;
              }
              // ************************JobSatisfaction********************
              if(this.predicitons[predict].WorkLifeBalance=="Bad"){
                this.predictionJson[count]["WorkLifeBalance"] = 1;
              }
              else if(this.predicitons[predict].WorkLifeBalance=="Good"){
                this.predictionJson[count]["WorkLifeBalance"] = 2;
              }
              else if(this.predicitons[predict].WorkLifeBalance=="Better"){
                this.predictionJson[count]["WorkLifeBalance"] = 3;
              }
              else if(this.predicitons[predict].WorkLifeBalance=="Best"){
                this.predictionJson[count]["WorkLifeBalance"] = 4;
              }
              count++;
              }
              console.log("Tweaked JSON");
              console.log(this.predictionJson);
              console.log('in component')
              this.emprecordService.getPrediction(this.predictionJson).subscribe(
              data => { console.log("DATA1>>"+data);this.emprecordService.predicitonarray= data; this.predicitonarray= data; console.log(data)},
              err => console.error(err),
              () => console.log('done getting prediction')
              );
              //console.log("DATA2 >>"+this.predicitonarray)
              //return this.predicitonarray;
          }
          console.log("file check")
          console.log(this.file)
          fileReader.readAsArrayBuffer(this.file);
      };
}
