import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employee } from './employee';

@Injectable({
providedIn: 'root'
})
export class EmprecordService {

private data:any={};
private rec:any;
private emp:Employee;
public predicitonarray

setData(data:any){

    this.emp = <Employee> {
    "EmployeeNumber" : data["EmployeeNumber"],
    "EmployeeName" : data["EmployeeName"],
    "Age" : data["Age"],
    "BusinessTravel" : data["BusinessTravel"],
    "Department" : data["Department"],
    "DistanceFromHome" : data["DistanceFromHome"],
    "EnvironmentSatisfaction" : data["EnvironmentSatisfaction"],
    "Gender" : data["Gender"],
    "JobInvolvement" : data["JobInvolvement"],
    "JobSatisfaction" : data["JobSatisfaction"],
    "MaritalStatus" : data["MaritalStatus"],
    "MonthlyIncome" : data["MonthlyIncome"],
    "NumCompaniesWorked" : data["NumCompaniesWorked"],
    "OverTime" : data["OverTime"],
    "PercentSalaryHike" : data["PercentSalaryHike"],
    "TrainingTimesLastYear" : data["TrainingTimesLastYear"],
    "WorkLifeBalance" : data["WorkLifeBalance"],
    "YearsInCurrentRole" : data["YearsInCurrentRole"],
    "AttritionValue" : data["AttritionValue"],
    "Reason" : data["Reason"]
};
    this.rec = data;

console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++data"+this.data["isBatch"]);
}
constructor(private http:HttpClient) {
    this.data["isBatch"]= false;
}

getPrediction(body) {
    console.log('in service class')
    console.log(body)
    this.data = this.http.post('http://localhost:9080/predict', body);
    console.log('data==')
    console.log(this.data)
    return this.data
} 

getData():any{
    console.log("Inside getData()"+this.data)
    return this.emp;
}
}

