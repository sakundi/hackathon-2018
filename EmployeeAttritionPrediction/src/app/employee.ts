export class Employee {
    EmployeeNumber: number
    EmployeeName: string
    Age: number
    BusinessTravel: number
    Department: number
    DistanceFromHome: number
    EnvironmentSatisfaction: number
    Gender: number
    JobInvolvement: number
    JobSatisfaction: number
    MaritalStatus: number
    MonthlyIncome: number
    NumCompaniesWorked: number
    OverTime: number
    PercentSalaryHike: number
    TrainingTimesLastYear: number
    WorkLifeBalance: number
    YearsInCurrentRole: number
    AttritionValue: number
    Reason: string
}
