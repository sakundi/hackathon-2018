# Employee Attrition Prediction

## Objective
As part of this hackathon we chose Employee Attrition Prediction challenge through which an HR can predict the attrition and the probable causes for the attrition. Our aim is to provide user friendly screens and cost effective solution to the problem. We’ll demonstrate you how we have arrived at our solution by walking you through various aspects of this solution.

## Overview
Employee Attrition is one of the major problem faced by organizations, and there is a huge cost is involved because of this, and in order to mitigate this problem, HR needs a tool through which he can predict the attrition and take preventive steps before hand.

We have developed an application which can do this, by leveraging the Python machine learning algorithm api provided by sklearn. The User Interface of this application is such that, a HR can enter details of a person and check if he is prone to attrition, not only that, he can upload a list of employees and perform a bulk attrition check.

The result of this attrition check is a probable percentage of attrition and the probable causes of the attrition.

## Architecture
Here is our architecture, we first train the model with existing data available with the organization and persist the model to the disk. Here, we are providing RESTful web service which calls predict method on the pre-trained persistence model. This RESTful API is based on Flask module on top of Python.

Here, we have an Angular application designed using Bootstrap running on top of Node JS which consumes the RESTful web service to predict the attrition.

For front-end we are using Angular with Bootstrap on top of Node JS, for backend we are using Flask and Sklearn modules on top of Python. We downloaded the IBM’s Employee Attrition dataset from Kaggle.

## Approach
So, how we are confident that our model performs well? We took step by step approach to prepare data, selecting algorithm.

Our dataset has around 35 features and inorder to improve the performance of our model we have to reduce the number of features which are not required. So, first we plotted heatmaps and removed one of the correlated feature from each of the correlation.
Then we molded data to replace text to numerics as prediction algorithm require numbers as part of it’s input for prediction. We also up-sample the attrition true records, as their ratio was far less.

As the model need to predict either yes or no which can be done by supervised algorithms, we eliminated algorithms that do not support supervised machine learning and binary classification.

We then selected GaussianNB, Logistic Regression, Decision Tree Classifier, Random Forest Classifier algorithms and compared their metrics and zeroed on to Random Forest Classifier as it was giving us 96% of accuracy, precision, recall and f-score.

Then we trained and tested it with 70-30 split and persisted the model using Pickle module, and provided a service which can query this model.

This is how we ensured that our model is robust.

## Software Requirements
1. Node JS 8+
3. Python 3+

## Installation
Once downloaded or cloned, open command prompt, change directory to /PythonService, type:

```
python predict.py
```
This will start the RESTful web service.

Now, open another command prompt, and change directory to /EmployeeAttritionPrediction, and type:

```
npm start
```
Open a browser and hit http://localhost:4200

## Tools & Technologies
NodeJS 8.11, Angular 6, Bootstrap 4, Python 3.6, Sklearn, Flask

## Team
1. Srinivasa Akundi
2. Sachin Jain
3. Rohit Raghunath
4. Rupali Dange
5. Surabhi Javkar
6. Sumeet Ram

## References / Credits to frameworks
- https://www.python.org/
- http://scikit-learn.org/
- http://getbootstrap.com/
- https://bootswatch.com/3/slate/
- https://angular.io/
- https://nodejs.org/
