# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 16:06:04 2018

@author: sacjain
"""

# Load libraries
import pandas as pd
import os

#Load the data
hr_data = pd.read_csv(os.getcwd()+"/employee_data.csv")

#Missing values check
hr_data.isnull().sum()

from sklearn.utils import resample

# Separate majority and minority classes
hr_data_majority = hr_data[hr_data.Attrition==0]
hr_data_minority = hr_data[hr_data.Attrition==1]
 
# Upsample minority class
hr_data_minority_upsampled = resample(hr_data_minority, 
                                 replace=True,     # sample with replacement
                                 n_samples=1233,    # to match majority class
                                 random_state=123) # reproducible results
 
# Combine majority class with upsampled minority class
hr_data_upsampled = pd.concat([hr_data_majority, hr_data_minority_upsampled])
 
# Display new class counts
hr_data_upsampled.Attrition.value_counts()
hr_data = hr_data_upsampled

hr_data = hr_data.drop(['EmployeeNumber'], axis = 1)

from sklearn.model_selection import train_test_split

target = hr_data['Attrition']
features = hr_data.drop('Attrition', axis = 1)

# Spliting into 70% for training set and 30% for testing set so we can see our accuracy
X_train, X_test, y_train, y_test = train_test_split(features, target, test_size=0.3, random_state=10)

#print("Original True  : {0} ({1:0.2f}%)".format(len(hr_data.loc[hr_data['Attrition'] == 0]), (len(hr_data.loc[hr_data['Attrition'] == 0])/len(hr_data.index)) * 100.0))
#print("Original False : {0} ({1:0.2f}%)".format(len(hr_data.loc[hr_data['Attrition'] == 1]), (len(hr_data.loc[hr_data['Attrition'] == 1])/len(hr_data.index)) * 100.0))
#print("")
#print("Training True  : {0} ({1:0.2f}%)".format(len(y_train[y_train[:] == 0]), (len(y_train[y_train[:] == 0])/len(y_train) * 100.0)))
#print("Training False : {0} ({1:0.2f}%)".format(len(y_train[y_train[:] == 1]), (len(y_train[y_train[:] == 1])/len(y_train) * 100.0)))
#print("")
#print("Test True      : {0} ({1:0.2f}%)".format(len(y_test[y_test[:] == 0]), (len(y_test[y_test[:] == 0])/len(y_test) * 100.0)))
#print("Test False     : {0} ({1:0.2f}%)".format(len(y_test[y_test[:] == 1]), (len(y_test[y_test[:] == 1])/len(y_test) * 100.0)))

from sklearn.ensemble import RandomForestClassifier

model = RandomForestClassifier(n_estimators=60)
model.fit(X_train,y_train)
from sklearn.metrics import accuracy_score
test_pred = model.predict(X_test)
print(accuracy_score(y_test, test_pred))

from sklearn.metrics import classification_report, roc_curve, auc
print(classification_report(y_test, test_pred))

fpr, tpr, thresholds = roc_curve(y_test, model.predict_proba(X_test)[:,1])
print("AUC")
print(auc(fpr, tpr))

import pickle

dest = os.path.join(os.getcwd(), 'model')

if not os.path.exists(dest):
  os.makedirs(dest)
 
pickle.dump(model, open(os.path.join(dest, 'emp_attr_pred.pkl'), 'wb'))