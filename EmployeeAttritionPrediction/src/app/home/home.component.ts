import { Component, OnInit } from '@angular/core';
import '@angular/router';
import { EditComponent } from '../edit/edit.component';
import { RouterModule, Routes, Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { EmprecordService } from '../emprecord.service';

import { PredictionService } from '../service/predictionService';
import { UploadService } from '../service/upload.service';
//import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {

  itemCount: number;
  btnText: string = "View";
  predicitonarray
  bulkUserData
  file
  data:any = {text: "example"};
  constructor(private route:ActivatedRoute, private router:Router, public uploadService: UploadService,
         private emprecordService:EmprecordService){

          router.events.subscribe( event => {

            if (event instanceof NavigationStart) {
                console.log('route')
            }
          });
         }

  ngOnInit() {
    console.log("in ngOnInit")
  }

  Upload() {
    if(this.uploadService.file != undefined) {
      this.uploadService.Upload()
    }
  }

  showEdit(emp){
      emp["isBatch"]=true;
      this.emprecordService.setData(emp)
  }






}
