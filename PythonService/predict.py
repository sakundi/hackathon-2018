import json
from input import getAttrition
from flask import Flask, jsonify, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/predict', methods=['GET','POST'])
def predict():
#    inputJson = '{Age:26, DistanceFromHome:25, EnvironmentSatisfaction:1, Gender:0, JobSatisfaction:3, MaritalStatus:0, MonthlyIncome:2293, PercentSalaryHike:12, PerformanceRating:3, TotalWorkingYears:1, WorkLifeBalance:2, YearsAtCompany:1, YearsSinceLastPromotion:0}'
#    return jsonify(inputJson)
    
#    inputJson = ['{"Age":50, "BusinessTravel":0, "Department":0, "DistanceFromHome":20, "EnvironmentSatisfaction":4, "Gender":1, "JobInvolvement":2, "JobSatisfaction":1, "MaritalStatus":2, "MonthlyIncome":10854, "NumCompaniesWorked":4, "OverTime":1, "PercentSalaryHike":13, "TrainingTimesLastYear":3, "WorkLifeBalance":3, "YearsInCurrentRole":2}', 
#                 '{"Age":50, "BusinessTravel":0, "Department":0, "DistanceFromHome":20, "EnvironmentSatisfaction":4, "Gender":1, "JobInvolvement":2, "JobSatisfaction":1, "MaritalStatus":2, "MonthlyIncome":10854, "NumCompaniesWorked":4, "OverTime":1, "PercentSalaryHike":13, "TrainingTimesLastYear":3, "WorkLifeBalance":3, "YearsInCurrentRole":2}']
#    return jsonify(inputJson)

    inputJson = request.json
    prediction = getAttrition(inputJson)  #json.dumps()
    print(prediction)
    return jsonify(prediction)

app.run(port=9080)