import { Component, OnInit, Input } from '@angular/core';
import { EmprecordService } from '../emprecord.service';
import { Router } from '@angular/router';
import { Employee } from '../employee';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  employeeForm:FormGroup
  private Age:FormControl
  private EmployeeNumber: FormControl
  private EmployeeName: FormControl
  private BusinessTravel: FormControl
  private Department: FormControl
  private DistanceFromHome: FormControl
  private EnvironmentSatisfaction: FormControl
  private Gender: FormControl
  private JobInvolvement: FormControl
  private JobSatisfaction: FormControl
  private MaritalStatus: FormControl
  private MonthlyIncome: FormControl
  private NumCompaniesWorked: FormControl
  private OverTime: FormControl
  private PercentSalaryHike: FormControl
  private TrainingTimesLastYear: FormControl
  private WorkLifeBalance: FormControl
  private YearsInCurrentRole: FormControl

  isYearsInCurrentRole: boolean
  isEmployeeNumber: boolean
  isEmployeeName: boolean
  isAge: boolean
  isDistanceFromHome: boolean
  isGender: boolean
  isMaritalStatus: boolean
  isNumCompaniesWorked: boolean

  public canvasWidth: number = 0
  public needleValue: number = 0
  public centralLabel: string
  public options
  public name = 'Attrition Risk'
  public bottomLabel
  public emp:Employee
  public original_emp:Employee
  public isBatch=false
  public reasons


  data:any = {text: "example"};

  constructor(private router:Router,
      public emprecordService:EmprecordService){
        //this.data = emprecordService.getData();
      }

  current: string = "Edit and Request again to check new Prediction"

  @Input() record: any

  ngOnInit() {
    this.emp = <Employee>{};
    this.Age = new FormControl(this.emp.Age, [Validators.required, Validators.pattern('[0-9].*')])
    this.EmployeeName = new FormControl(this.emp.EmployeeName)
    this.EmployeeNumber = new FormControl(this.emp.EmployeeNumber)
    this.BusinessTravel = new FormControl(this.emp.BusinessTravel)
    this.Department = new FormControl(this.emp.Department)
    this.DistanceFromHome = new FormControl(this.emp.DistanceFromHome, [Validators.required, Validators.pattern('[0-9].*')])
    this.EnvironmentSatisfaction = new FormControl(this.emp.EnvironmentSatisfaction)
    this.Gender = new FormControl(this.emp.Gender)
    this.JobInvolvement = new FormControl(this.emp.JobInvolvement)
    this.JobSatisfaction = new FormControl(this.emp.JobSatisfaction)
    this.MaritalStatus = new FormControl(this.emp.MaritalStatus)
    this.MonthlyIncome = new FormControl(this.emp.MonthlyIncome, [Validators.required, Validators.pattern('[0-9].*')])
    this.NumCompaniesWorked = new FormControl(this.emp.NumCompaniesWorked, [Validators.required, Validators.pattern('[0-9].*')])
    this.OverTime = new FormControl(this.emp.OverTime)
    this.PercentSalaryHike = new FormControl(this.emp.PercentSalaryHike, [Validators.required, Validators.pattern('[0-9].*')])
    this.TrainingTimesLastYear = new FormControl(this.emp.TrainingTimesLastYear, [Validators.required, Validators.pattern('[0-9].*')])
    this.WorkLifeBalance = new FormControl(this.emp.WorkLifeBalance)
    this.YearsInCurrentRole = new FormControl(this.emp.YearsInCurrentRole, [Validators.required, Validators.pattern('[0-9].*')])

    this.employeeForm = new FormGroup({
      Age: this.Age,
      EmployeeName: this.EmployeeName,
      EmployeeNumber: this.EmployeeNumber,
      BusinessTravel: this.BusinessTravel,
      Department: this.Department,
      DistanceFromHome: this.DistanceFromHome,
      EnvironmentSatisfaction: this.EnvironmentSatisfaction,
      Gender: this.Gender,
      JobInvolvement: this.JobInvolvement,
      JobSatisfaction: this.JobSatisfaction,
      MaritalStatus: this.MaritalStatus,
      MonthlyIncome: this.MonthlyIncome,
      NumCompaniesWorked: this.NumCompaniesWorked,
      OverTime: this.OverTime,
      PercentSalaryHike: this.PercentSalaryHike,
      TrainingTimesLastYear: this.TrainingTimesLastYear,
      WorkLifeBalance: this.WorkLifeBalance,
      YearsInCurrentRole: this.YearsInCurrentRole
    });

    this.canvasWidth = 300

    this.options = {
      hasNeedle: true,
      needleColor: 'gray',
      needleUpdateSpeed: 1000,
      arcColors: ['green','yellow','red'],
      arcDelimiters: [33,66],
      rangeLabel: ['0', '100'],
      needleStartValue: 50
    }

    this.emp = this.emprecordService.getData();
    this.original_emp = Object.assign({}, this.emp);
    if(this.emp != undefined) {
      this.isBatch = true;
      this.needleValue = Math.round(this.emp.AttritionValue*100);
      this.bottomLabel = Math.round(this.emp.AttritionValue*100);
      this.reasons = this.emp.Reason.split(",");
      for(var r=0; r < this.reasons.length; r++) {
        this.reasons[r] = this.reasons[r].replace(/([A-Z])/g, ' $1').trim();
      }


      this.employeeForm.patchValue({
        Age: this.emp.Age,
        EmployeeName: this.emp.EmployeeName,
        EmployeeNumber: this.emp.EmployeeNumber,
        BusinessTravel: this.emp.BusinessTravel,
        Department: this.emp.Department,
        DistanceFromHome: this.emp.DistanceFromHome,
        EnvironmentSatisfaction: this.emp.EnvironmentSatisfaction,
        Gender: this.emp.Gender,
        JobInvolvement: this.emp.JobInvolvement,
        JobSatisfaction: this.emp.JobSatisfaction,
        MaritalStatus: this.emp.MaritalStatus,
        MonthlyIncome: this.emp.MonthlyIncome,
        NumCompaniesWorked: this.emp.NumCompaniesWorked,
        OverTime: this.emp.OverTime,
        PercentSalaryHike: this.emp.PercentSalaryHike,
        TrainingTimesLastYear: this.emp.TrainingTimesLastYear,
        WorkLifeBalance: this.emp.WorkLifeBalance,
        YearsInCurrentRole: this.emp.YearsInCurrentRole
      });

      this.isYearsInCurrentRole = true;
      this.isEmployeeNumber = true;
      this.isEmployeeName = true;
      this.isAge = true;
      this.isDistanceFromHome = true;
      this.isNumCompaniesWorked = true;
      this.employeeForm.get('Gender').disable();
      this.employeeForm.get('MaritalStatus').disable();

    }else{
        this.needleValue = 0;
        this.bottomLabel = 0;
        this.emp = <Employee>{};
      }
    console.log("Data value ==> "+this.data);
    console.log(this.data);

  }

    //newData:any
    item:any
    array = []



    checkAttrition(){
      console.log("Inside checkAttrition()");
      //newData = [].concat(newData)
      console.log(this.emp)


      this.emp = {
        Age: Number(this.emp.Age),
        EmployeeName: this.emp.EmployeeName,
        EmployeeNumber: this.emp.EmployeeNumber,
        BusinessTravel: Number(this.emp.BusinessTravel),
        Department: Number(this.emp.Department),
        DistanceFromHome: Number(this.emp.DistanceFromHome),
        EnvironmentSatisfaction: Number(this.emp.EnvironmentSatisfaction),
        Gender: Number(this.emp.Gender),
        JobInvolvement: Number(this.emp.JobInvolvement),
        JobSatisfaction: Number(this.emp.JobSatisfaction),
        MaritalStatus: Number(this.emp.MaritalStatus),
        MonthlyIncome: Number(this.emp.MonthlyIncome),
        NumCompaniesWorked: Number(this.emp.NumCompaniesWorked),
        OverTime: Number(this.emp.OverTime),
        PercentSalaryHike: Number(this.emp.PercentSalaryHike),
        TrainingTimesLastYear: Number(this.emp.TrainingTimesLastYear),
        WorkLifeBalance: Number(this.emp.WorkLifeBalance),
        YearsInCurrentRole: Number(this.emp.YearsInCurrentRole),
        AttritionValue: 0,
        Reason:''
      };

      this.data =  this.emprecordService.getPrediction([].concat(this.emp)).subscribe(
        data => {
          console.log("DATA1232>>");
          console.log(data[0]);
          this.data= data[0]
          this.needleValue = Math.round(this.data.AttritionValue*100);
          this.bottomLabel = Math.round(this.data.AttritionValue*100);
          this.reasons = this.data.Reason.split(",");
          for(var r=0; r < this.reasons.length; r++) {
            this.reasons[r] = this.reasons[r].replace(/([A-Z])/g, ' $1').trim();
          }

          },
        err => console.error(err),
        () => console.log('done getting prediction')
        );
    }

    reset(){
      this.emp = Object.assign({}, this.original_emp);
      this.reasons = this.emp.Reason.split(",");
      for(var r=0; r < this.reasons.length; r++) {
        this.reasons[r] = this.reasons[r].replace(/([A-Z])/g, ' $1').trim();
      }
      console.log(this.emp.YearsInCurrentRole);

      this.Age.setValue(this.emp.Age);
      this.EmployeeName.setValue(this.emp.EmployeeName);
      this.EmployeeNumber.setValue(this.emp.EmployeeNumber);
      this.BusinessTravel.setValue(this.emp.BusinessTravel);
      this.Department.setValue(this.emp.Department);
      this.DistanceFromHome.setValue(this.emp.DistanceFromHome);
      this.EnvironmentSatisfaction.setValue(this.emp.EnvironmentSatisfaction);
      this.Gender.setValue(this.emp.Gender);
      this.JobInvolvement.setValue(this.emp.JobInvolvement);
      this.JobSatisfaction.setValue(this.emp.JobSatisfaction);
      this.MaritalStatus.setValue(this.emp.MaritalStatus);
      this.MonthlyIncome.setValue(this.emp.MonthlyIncome);
      this.NumCompaniesWorked.setValue(this.emp.NumCompaniesWorked);
      this.OverTime.setValue(this.emp.OverTime);
      this.PercentSalaryHike.setValue(this.emp.PercentSalaryHike);
      this.TrainingTimesLastYear.setValue(this.emp.TrainingTimesLastYear);
      this.WorkLifeBalance.setValue(this.emp.WorkLifeBalance);
      this.YearsInCurrentRole.setValue(this.emp.YearsInCurrentRole);

      this.isYearsInCurrentRole = true;
      this.isEmployeeNumber = true;
      this.isEmployeeName = true;
      this.isAge = true;
      this.isDistanceFromHome = true;
      this.isNumCompaniesWorked = true;
      this.employeeForm.get('Gender').disable();
      this.employeeForm.get('MaritalStatus').disable();
    }

    clear() {
      this.isYearsInCurrentRole = false;
      this.isEmployeeNumber = false;
      this.isEmployeeName = false;
      this.isAge = false;
      this.isDistanceFromHome = false;
      this.isNumCompaniesWorked = false;
      this.employeeForm.get('Gender').enable();
      this.employeeForm.get('MaritalStatus').enable();
      this.reasons = {};
      this.needleValue = 0
      this.bottomLabel = 0
    }
}
